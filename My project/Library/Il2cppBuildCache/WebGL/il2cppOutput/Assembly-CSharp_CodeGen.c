﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void LevelGenerator::Start()
extern void LevelGenerator_Start_mE77C8E10B10DC67F9AEBD58979C99AF4801DE6AE (void);
// 0x00000002 System.Void LevelGenerator::Update()
extern void LevelGenerator_Update_m8B4F90DD746D10051763DCDB5C2A2D49E974A650 (void);
// 0x00000003 System.Void LevelGenerator::.ctor()
extern void LevelGenerator__ctor_mA92E567AAACDCB71B5168F43CDFD41F69938CB59 (void);
// 0x00000004 System.Void PlatformJump::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlatformJump_OnCollisionEnter2D_m26B854DCAB2429ACF38AE6B88ACA7CDF57EF9E25 (void);
// 0x00000005 System.Void PlatformJump::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlatformJump_OnTriggerEnter2D_m9CB098B4D21A396B4AC91C4CCD2FE537E731998B (void);
// 0x00000006 System.Void PlatformJump::.ctor()
extern void PlatformJump__ctor_mEBF133812333D084785A9A4BE57C404B709A3430 (void);
// 0x00000007 System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x00000008 System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x00000009 System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088 (void);
// 0x0000000A System.Void Player::checkBoundary(UnityEngine.Vector3)
extern void Player_checkBoundary_m16089E0D08698D8890C776566C7F96C6269DEABD (void);
// 0x0000000B System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x0000000C System.Void cameraFollow::Start()
extern void cameraFollow_Start_mDD48E2DE1927A8976E3C67526C2635C53C3890F2 (void);
// 0x0000000D System.Void cameraFollow::LateUpdate()
extern void cameraFollow_LateUpdate_m6742AFBA53A40DA04E8B4FCB5D98C744BCD3A403 (void);
// 0x0000000E System.Void cameraFollow::.ctor()
extern void cameraFollow__ctor_mD5FCB85DBD7D622D7BB372333945BB9E0C533C7B (void);
static Il2CppMethodPointer s_methodPointers[14] = 
{
	LevelGenerator_Start_mE77C8E10B10DC67F9AEBD58979C99AF4801DE6AE,
	LevelGenerator_Update_m8B4F90DD746D10051763DCDB5C2A2D49E974A650,
	LevelGenerator__ctor_mA92E567AAACDCB71B5168F43CDFD41F69938CB59,
	PlatformJump_OnCollisionEnter2D_m26B854DCAB2429ACF38AE6B88ACA7CDF57EF9E25,
	PlatformJump_OnTriggerEnter2D_m9CB098B4D21A396B4AC91C4CCD2FE537E731998B,
	PlatformJump__ctor_mEBF133812333D084785A9A4BE57C404B709A3430,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088,
	Player_checkBoundary_m16089E0D08698D8890C776566C7F96C6269DEABD,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	cameraFollow_Start_mDD48E2DE1927A8976E3C67526C2635C53C3890F2,
	cameraFollow_LateUpdate_m6742AFBA53A40DA04E8B4FCB5D98C744BCD3A403,
	cameraFollow__ctor_mD5FCB85DBD7D622D7BB372333945BB9E0C533C7B,
};
static const int32_t s_InvokerIndices[14] = 
{
	867,
	867,
	867,
	750,
	750,
	867,
	867,
	867,
	867,
	768,
	867,
	867,
	867,
	867,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	14,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
